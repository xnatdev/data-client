/*
 * data-client: org.nrg.xnat.client.data.TestXnatDataClient
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.client.data;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.junit.Rule;
import org.junit.Test;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.Assert.assertEquals;

/**
 * TestXnatDataClient class.
 *
 * @author Rick Herrick
 */
// The WireMock stuff is documented here: http://wiremock.org/getting-started.html
public class TestXnatDataClient {
    @Rule
    public WireMockRule _wireMockRule = new WireMockRule(8089);

    @Test
    public void testSimpleGet() throws Exception {
        stubFor(get(urlEqualTo("/my/resource"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "text/xml")
                        .withBody("<response>Some content</response>")));
        assertEquals(0, new XnatDataClient("http://localhost:8089/my/resource").launch());
    }
}
