/*
 * data-client: org.nrg.xnat.client.data.TestXnatDataClientPintoBean
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.client.data;

import org.junit.Test;
import org.nrg.framework.pinto.PintoApplication;
import org.nrg.framework.pinto.PintoException;

import static org.junit.Assert.*;

@PintoApplication(value = TestXnatDataClientPintoBean.TITLE, copyright = TestXnatDataClientPintoBean.COPYRIGHT, introduction = TestXnatDataClientPintoBean.INTRODUCTION)
public class TestXnatDataClientPintoBean {
    static final String TITLE = "TestXnatDataClientPintoBean Unit Tests";
    static final String COPYRIGHT = "(c) 2016 Washington University School of Medicine";
    static final String INTRODUCTION = "Hi there!";

    @Test
    public void testHelp() {
        XnatDataClientPintoBean bean = null;
        try {
            bean = new XnatDataClientPintoBean(this, new String[] { "-h" });
        } catch (PintoException exception) {
            fail("Found an exception in what should have been a valid parameter [" + exception.getParameter() + "]: " + exception.getType() + " " + exception.getMessage());
        }
        assertTrue(bean.getHelp());
        assertFalse(bean.getVersion());
        assertFalse(bean.getShouldContinue());
    }
    @Test
    public void testVersion() {
        XnatDataClientPintoBean bean = null;
        try {
            bean = new XnatDataClientPintoBean(this, new String[] { "-v" });
        } catch (PintoException exception) {
            fail("Found an exception in what should have been a valid parameter [" + exception.getParameter() + "]: " + exception.getType() + " " + exception.getMessage());
        }
        assertTrue(bean.getVersion());
        assertFalse(bean.getHelp());
        assertFalse(bean.getShouldContinue());
    }
}
